<?php 

namespace App\Controllers;

use App\Model\NewsModel;

class News extends BaseController{

    public function index(){
        $model = new NewsModel();
        $data = [
            'news'  => $model->getNews(),
            'title' => 'news archive'
        ];
       
        // return view('templates/header', $data)
        //     . view('news/overview')
        //     . view('templates/footer');

        // return view('welcome_message');
    }
    public function view($slug = null){
        $model = new NewsModel();
        $data['news'] = $model->getNews($slug);
    }



}